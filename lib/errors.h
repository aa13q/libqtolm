#ifndef ERRORS_H
#define ERRORS_H

#include <QString>
#include <stdexcept>

namespace QtOlm {
class UnknownError : public std::exception {
 public:
  using std::exception::exception;
};
class InvalidArgument : public std::invalid_argument {
 public:
  using std::invalid_argument::invalid_argument;
};
class OlmError : public std::runtime_error {
 public:
  using std::runtime_error::runtime_error;
};
class EntropyError : public OlmError {
 public:
  using OlmError::OlmError;
};
class BufferError : public OlmError {
 public:
  using OlmError::OlmError;
};
class Base64Error : public OlmError {
 public:
  using OlmError::OlmError;
};
class SignatureError : public OlmError {
 public:
  using OlmError::OlmError;
};
class AccountKeyError : public OlmError {
 public:
  using OlmError::OlmError;
};
class SessionKeyError : public OlmError {
 public:
  using OlmError::OlmError;
};
class MessageError : public OlmError {
 public:
  using OlmError::OlmError;
};
class PickleError : public OlmError {
 public:
  using OlmError::OlmError;
};
}  // namespace QtOlm

#endif  // ERRORS_H
