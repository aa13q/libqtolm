#ifndef UTILS_H
#define UTILS_H

#include "olm/olm.h"

#include <cerrno>
#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <tuple>

#include <QByteArray>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    #include <QRandomGenerator>
#else
    #ifndef QT_NO_QT_INCLUDE_WARN
        #pragma message "WARNING: You're using Qt<5.10. Using fallback options instead of QRandomGenerator."
        #pragma message "WARNING: It's highly recommended to update your Qt package!"
    #endif // QT_NO_QT_INCLUDE_WARN
#endif // Qt 5.10
#include <QString>

#include "errors.h"

namespace QtOlm {
static OlmUtility* utility = olm_utility(new uint8_t[olm_utility_size()]);

static QByteArray getRandomData(int buffer_size) {
  QByteArray buffer(buffer_size, '0');
  #if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    std::generate(buffer.begin(), buffer.end(), *QRandomGenerator::system());
  #else
    std::generate(buffer.begin(), buffer.end(), std::rand);
  #endif // Qt 5.10
  return buffer;
}

static void ed25519Verify(QByteArray key,
                          QString message,
                          QByteArray signature) {
  std::string msg = message.toStdString();
  if (olm_ed25519_verify(utility, key.data(), key.length(), msg.data(),
                         msg.length(), signature.data(),
                         signature.length()) == olm_error())
    throw new SignatureError("Signature is invalid");
}
}  // namespace QtOlm

#endif  // UTILS_H
